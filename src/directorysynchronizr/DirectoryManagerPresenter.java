package directorysynchronizr;

import java.awt.Color;
import java.awt.Desktop;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.SystemColor;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.util.ArrayList;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.Timer;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.plaf.basic.BasicProgressBarUI;

public class DirectoryManagerPresenter extends JFrame {

	private static final long serialVersionUID = 1L;
	public static final String VERSION = "v1.3.2";
	private static final String NEWEST_VERSION_URL = "https://drive.google.com/file/d/1BJxHp-ZTw8hJdYKxj4Z7oZskMEyU8PPA";
	public static String NEWEST_VERSION = "";
	public static final String APP_FOLDER_NAME = "DirectorySynchronizR";
	public static final String RECENT_DIRECTORIES_FILENAME = "Recent_Directories.list";
	public static final String LOGS_FOLDER_NAME = "Summary-Logs";
	private JPanel contentPane;
	private JTextField tfFilePathMaster;
	private JTextField tfFilePathSlave;
	private JButton btnBrowseMaster;
	private JButton btnBrowseSlave;
	private JButton btnSynchronizeSlave;
	private JButton btnDeleteEmptyFolders;
	private JScrollPane scrollPane;
	private String currentPathMaster = System.getProperty("user.home") + "\\Music\\iTunes";
	private String currentPathSlave = "M:\\iTunes";
	private static Timer timer;
	private JProgressBar progressBar;
	private DirectoryManager directoryManager = new DirectoryManager();
	private static JTextArea taskOutput;
	protected static long startTimeMillis;
	private JMenuBar menuBar;
	private JMenu mnSettings;
	private JMenu mnHelp;
	private JCheckBoxMenuItem cbMoveToTrash;
	private JMenuItem miAbout;
	private JMenuItem miCheckForUpdates;
	protected long lastVersionCheckTimeMillis = 0;
	private JCheckBoxMenuItem cbItunesDeleteFoldersWithOnlyHiddenImageFiles;
	private JSeparator separator_1;
	private JCheckBoxMenuItem cbAlsoSynchronizeBackToMaster;
	private String averageCopySpeedString = "";
	private Logs logFrame = new Logs("");
	private String timeLeftString = "";
	private long progressbarLastUpdatedMillis;
	private DirectoryManagerPresenter directoryManagerPresenter = this;

	Thread synchronizationThread = new Thread();
	private static boolean jobFinishInitiated = false;
	private JButton btnToggleMaster;
	private JButton btnToggleSlave;
	public static String defaultDirectory = new JFileChooser().getFileSystemView().getDefaultDirectory().toString();
	public static String appFolderPath = "";
	private static File recentDirectoriesFile = null;
	private static ArrayList<String> recentDirectories = new ArrayList<String>();
	private static int recentDirectoryIteratorMaster = 0;
	private static int recentDirectoryIteratorSlave = 0;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
				} catch (ClassNotFoundException ex) {
					java.util.logging.Logger.getLogger(DirectoryManagerPresenter.class.getName())
							.log(java.util.logging.Level.SEVERE, null, ex);
				} catch (InstantiationException ex) {
					java.util.logging.Logger.getLogger(DirectoryManagerPresenter.class.getName())
							.log(java.util.logging.Level.SEVERE, null, ex);
				} catch (IllegalAccessException ex) {
					java.util.logging.Logger.getLogger(DirectoryManagerPresenter.class.getName())
							.log(java.util.logging.Level.SEVERE, null, ex);
				} catch (javax.swing.UnsupportedLookAndFeelException ex) {
					java.util.logging.Logger.getLogger(DirectoryManagerPresenter.class.getName())
							.log(java.util.logging.Level.SEVERE, null, ex);
				}
				DirectoryManagerPresenter frame = new DirectoryManagerPresenter();
				frame.pack();
				frame.setVisible(true);
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public DirectoryManagerPresenter() {
		// this.setContentPane(new JLabel(new
		// ImageIcon(getClass().getResource("/pictures/tonyc.png"))));
		init();
		getContentPane().setBackground(Color.DARK_GRAY);

		try {
			ImageIcon img = new ImageIcon(getClass().getResource("/pictures/icon.png"));
			this.setIconImage(img.getImage());
		} catch (Exception e) {
			e.printStackTrace();
		}

		// Create a timer.
		timer = new Timer(10, new ActionListener() {
			double averageCopySpeedMBPerSec = 0;

			public void actionPerformed(ActionEvent evt) {
				if (!DirectoryManager.done()) {

					progressBar.setValue((int) DirectoryManager.getCurrent());
//					progressBar.setMaximum((int) directoryManager.getLengthOfTask());
					averageCopySpeedMBPerSec = (Math
							.round((DirectoryManager.getCurrentAverageCopySpeedInKiloBytesPerSecond() / 1000.0) * 100.0)
							/ 100.0);

					if (System.currentTimeMillis() - progressbarLastUpdatedMillis > 1000) {
						if (averageCopySpeedMBPerSec != 0.0) {
							averageCopySpeedString = " (" + averageCopySpeedMBPerSec + " MB/s)";
						}
						timeLeftString = DirectoryManager.getTimeLeftString();
						progressbarLastUpdatedMillis = System.currentTimeMillis();
					}

					progressBar.setString(String
							.valueOf(Math.round(Double.valueOf(DirectoryManager.getCurrent())
									/ Double.valueOf(directoryManager.getLengthOfTask()) * 100))
							+ " %" + averageCopySpeedString + " " + timeLeftString);
//					if (!DirectoryManager.done() && !DirectoryManager.getMessageBuffer().isEmpty()) {
//						taskOutput.append(
//								(DirectoryManager.getCurrent() == 0 ? "" : "\n") + DirectoryManager.getMessageBuffer());
//						taskOutput.setText(
//								(DirectoryManager.getCurrent() == 0 ? "" : "\n") + DirectoryManager.getMessageBuffer());
//						logFrame.setLogText(taskOutput.getText() != null ? taskOutput.getText() : "");
//						DirectoryManager.clearMessageBuffer();
//					}
				}
//				taskOutput.setCaretPosition(taskOutput.getDocument().getLength());
				if (!jobFinishInitiated && (DirectoryManager.done() || DirectoryManager.isEmptyFolderDeleteJobDone())) {
					System.out.println("Stopping timer - " + System.currentTimeMillis() / 1000);
					((Timer) evt.getSource()).stop();
					timer.stop();
					System.out.println("Timer stopped - " + System.currentTimeMillis() / 1000);
					jobFinishInitiated = true;
					Toolkit.getDefaultToolkit().beep();
					Long timeNeeded = System.currentTimeMillis() - startTimeMillis;
					String strTimeNeeded = (timeNeeded > 60000
							? Math.round(((((double) timeNeeded) / 1000.0) / 60.0)) + " min "
							: "") + Math.round(((((double) timeNeeded) / 1000.0) % 60.0)) + " secs";

					String completedOrCanceled = "Completed. ";
					long processedFiles = directoryManager.getLengthOfTask();
					if (DirectoryManager.isUserCanceled()) {
						completedOrCanceled = "Canceled by User. ";
						processedFiles = DirectoryManager.getCanceledProgress() + 1;
					}
					taskOutput.append(DirectoryManager.getCompletionSummaryAndWriteLogFile());
					if (DirectoryManager.isSynchronizeSwitchOn()) {
						String strMasterFilesReplaced = "";
						if (cbAlsoSynchronizeBackToMaster.isSelected()) {
							strMasterFilesReplaced = DirectoryManager.getFilesReplacedInMaster()
									+ " Master files updated, ";
						}

//						taskOutput.append(
//								(DirectoryManager.getCurrent() == 0 ? "" : "\n") + DirectoryManager.getMessageBuffer());
//						DirectoryManager.clearMessageBuffer();
						taskOutput.append("\n\n" + completedOrCanceled + processedFiles + " files processed. "
								+ DirectoryManager.getFilesCopied() + " master files copied, "
								+ DirectoryManager.getFilesReplaced() + " slave files updated, "
								+ strMasterFilesReplaced + DirectoryManager.getFilesDeleted()
								+ " files deleted. Finished after " + strTimeNeeded + ". Average copy speed: "
								+ averageCopySpeedMBPerSec + " MB/s");
					} else {
						taskOutput.append("\nCompleted. " + processedFiles + " directories processed. "
								+ DirectoryManager.getDeletedEmptyDirectoriesCounter()
								+ " empty directories deleted. Finished after " + strTimeNeeded + ".");
					}
					taskOutput.setCaretPosition(taskOutput.getDocument().getLength());
//					logFrame.setLogText(taskOutput.getText() != null ? taskOutput.getText() : "");
					try {
						System.out.println("Sleep 500");
						Thread.sleep(500);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					System.out.println("Resetting variables");
					DirectoryManager.resetVariables();
					System.out.println("Reactivating GUI");
					mnSettings.setEnabled(true);
					btnToggleMaster.setEnabled(true);
					btnToggleSlave.setEnabled(true);
					btnDeleteEmptyFolders.setEnabled(true);
					btnSynchronizeSlave.setEnabled(true);
					btnSynchronizeSlave.setText("Synchronize Slave with Master");
					btnDeleteEmptyFolders.setText("Delete Empty Folders in Master Directory");
					tfFilePathMaster.setEnabled(true);
					tfFilePathMaster.setEditable(true);
					btnBrowseMaster.setEnabled(true);
					tfFilePathSlave.setEnabled(true);
					tfFilePathSlave.setEditable(true);
					btnBrowseSlave.setEnabled(true);
					progressBar.setValue(progressBar.getMinimum());
					progressBar.setString("");
					synchronizationThread = null;
					System.out.println("Done");
				}
			}
		});

		readRecentDirectoriesFile();

	}

	private static void readRecentDirectoriesFile() {

		// Create if not exists

		defaultDirectory = new JFileChooser().getFileSystemView().getDefaultDirectory().toString();
		appFolderPath = defaultDirectory + File.separator + APP_FOLDER_NAME;
		recentDirectoriesFile = new File(appFolderPath + File.separator + RECENT_DIRECTORIES_FILENAME);

		if (!new File(appFolderPath).exists()) {
			try {
				Files.createDirectories(new File(appFolderPath).toPath());
			} catch (IOException e) {
				e.printStackTrace();
			}
			System.out.println(appFolderPath);
		}
		try {
			recentDirectoriesFile.createNewFile();
		} catch (IOException e) {
			e.printStackTrace();
		}

		try {
			recentDirectories = (ArrayList<String>) Files.readAllLines(recentDirectoriesFile.toPath());

			for (String line : recentDirectories) {
				System.out.println(line);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private static void addNewRecentDirectoryIfNotExists(String path) {
		if (!recentDirectories.contains(path)) {
			recentDirectories.add(path);
			writeNewRecentDirectoryIfNotExists(path);
		}

	}

	private static void writeNewRecentDirectoryIfNotExists(String path) {
		FileOutputStream fos = null;
		BufferedReader br = null;
		try {
			fos = new FileOutputStream(recentDirectoriesFile, true);
			br = new BufferedReader(new FileReader(recentDirectoriesFile));
			if (br.readLine() == null) {
				fos.write((path).getBytes());
			} else {
				fos.write(("\n" + path).getBytes());
			}
			br.close();
			fos.close();
		} catch (Exception e) {
			e.printStackTrace();
			try {
				if (fos != null)
					fos.close();

				if (br != null)
					br.close();

			} catch (Exception e1) {
			}
		}
	}

	private void init() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 500, 632);
		this.setLocationRelativeTo(null);

		menuBar = new JMenuBar();
		setJMenuBar(menuBar);

		mnSettings = new JMenu("Settings");
		menuBar.add(mnSettings);

		cbMoveToTrash = new JCheckBoxMenuItem("Move to Trash Bin When Deleting Slave Files");

		cbItunesDeleteFoldersWithOnlyHiddenImageFiles = new JCheckBoxMenuItem(
				"iTunes: Delete Folders With Only Hidden Image Files");
		cbItunesDeleteFoldersWithOnlyHiddenImageFiles.setSelected(true);
		mnSettings.add(cbItunesDeleteFoldersWithOnlyHiddenImageFiles);

		separator_1 = new JSeparator();
		mnSettings.add(separator_1);
		mnSettings.add(cbMoveToTrash);

		cbAlsoSynchronizeBackToMaster = new JCheckBoxMenuItem("Also Synchronize Back to Master If Slave File is Newer");
		cbAlsoSynchronizeBackToMaster.setSelected(false);
		mnSettings.add(cbAlsoSynchronizeBackToMaster);

		mnHelp = new JMenu("Help");
		menuBar.add(mnHelp);

		miAbout = new JMenuItem("About");
		miAbout.addActionListener(new java.awt.event.ActionListener() {
			@Override
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				System.out.println("asdasd");
				try {
					About about = new About();
					about.setLocationRelativeTo(directoryManagerPresenter);
					about.setVisible(true);
				} catch (URISyntaxException e1) {
					e1.printStackTrace();
				}
			}
		});

		miCheckForUpdates = new JMenuItem("Check for Updates");
		miCheckForUpdates.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				if (NEWEST_VERSION.length() < 2
						|| ((System.currentTimeMillis() - lastVersionCheckTimeMillis) / 1000) > 60) {
					NEWEST_VERSION = getNewestVersionNumber();
					lastVersionCheckTimeMillis = System.currentTimeMillis();
				}
				// miUpdateAvailable.setVisible(!NEWEST_VERSION.equals(VERSION));
				if (NEWEST_VERSION.equals(VERSION)) {
					JOptionPane.showMessageDialog(directoryManagerPresenter, "This version is up to date.");
				} else {
					Object[] options = { "Yes, please!", "Maybe later" };
					int userChoice = JOptionPane.showOptionDialog(directoryManagerPresenter,
							"New version available! (" + NEWEST_VERSION + ").\nDownload it now?", "Updates",
							JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, options, options[0]);
					if (userChoice == JOptionPane.YES_OPTION) {
						try {
							Desktop.getDesktop().browse(new URI(
									"https://drive.google.com/uc?id=1BJxHp-ZTw8hJdYKxj4Z7oZskMEyU8PPA&export=download"));
						} catch (IOException | URISyntaxException ex) {
							System.out.println("Could not open update page.");
						}
					}
				}

			}
		});

		mnHelp.add(miCheckForUpdates);
		mnHelp.add(miAbout);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		// contentPane.setLayout(new GridLayout(11, 4, 2, 2));
		contentPane.setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
		c.insets = new Insets(0, 0, 5, 0);
		int paddingTop = 8;
		int width = 150;

		c.fill = GridBagConstraints.HORIZONTAL;
		c.ipadx = width;
		c.gridx = 0;
		c.gridy = 0;
		// FilePath Master
		JLabel lblFPMaster = new JLabel("Master Directory Path");
		lblFPMaster.setForeground(Color.LIGHT_GRAY);
		lblFPMaster.setFont(new Font("Tahoma", Font.BOLD, 18));
		contentPane.add(lblFPMaster, c);

		c = new GridBagConstraints();
		c.fill = GridBagConstraints.HORIZONTAL;
		c.insets = new Insets(8, 0, 5, 0);
		c.ipadx = width;
		c.ipady = 8;
		c.gridx = 0;
		c.gridy = 1;
		tfFilePathMaster = new JTextField(System.getProperty("user.home") + "\\Music\\iTunes");
		tfFilePathMaster.setBackground(new Color(153, 255, 153));
		tfFilePathMaster.setForeground(Color.BLACK);
		tfFilePathMaster.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if (e.getClickCount() == 2 && !e.isConsumed()) {
					e.consume();
					browseMasterPath();
				}
			}
		});
		contentPane.add(tfFilePathMaster, c);
		tfFilePathMaster.getDocument().addDocumentListener(new DocumentListener() {
			void changeMasterDirectory() {
//				if ((new File(tfFilePathMaster.getText())).exists()) {
				currentPathMaster = tfFilePathMaster.getText().trim();
//				} else {
//					currentPathMaster = "";
//				}
//				System.out.println("currentPathMaster=" + currentPathMaster);
			}

			public void changedUpdate(DocumentEvent e) {
				changeMasterDirectory();
			}

			public void removeUpdate(DocumentEvent e) {
				changeMasterDirectory();
			}

			public void insertUpdate(DocumentEvent e) {
				changeMasterDirectory();
			}
		});

		c = new GridBagConstraints();
		c.fill = GridBagConstraints.HORIZONTAL;
		c.insets = new Insets(8, 0, 5, 0);
		c.ipadx = width;
		c.ipady = 15;
		c.gridx = 0;
		c.gridy = 2;
		btnBrowseMaster = new JButton();
		btnBrowseMaster.setForeground(Color.BLACK);
		btnBrowseMaster.setBackground(Color.WHITE);
		btnBrowseMaster.setFont(new Font("Tahoma", Font.PLAIN, 14));
		btnBrowseMaster.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				browseMasterPath();
			}
		});

		btnBrowseMaster.setText("Browse for Master Directory");
		contentPane.add(btnBrowseMaster, c);

		btnToggleMaster = new JButton("Toggle Recent");
		btnToggleMaster.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {

				if (recentDirectories.isEmpty())
					return;

				String directory = recentDirectories.get(recentDirectoryIteratorMaster);

//				if (new File(directory).exists()) {
				tfFilePathMaster.setText(directory);
				currentPathMaster = directory;
//				}

				if (recentDirectoryIteratorMaster >= recentDirectories.size() - 1) {
					recentDirectoryIteratorMaster = 0;
				} else {
					recentDirectoryIteratorMaster++;
				}
			}
		});
		btnToggleMaster.setHorizontalAlignment(SwingConstants.RIGHT);
		GridBagConstraints gbc_btnToggleMaster = new GridBagConstraints();
		gbc_btnToggleMaster.insets = new Insets(0, 0, 5, 0);
		gbc_btnToggleMaster.gridx = 0;
		gbc_btnToggleMaster.gridy = 3;
		contentPane.add(btnToggleMaster, gbc_btnToggleMaster);

		c = new GridBagConstraints();
		c.fill = GridBagConstraints.HORIZONTAL;
		c.insets = new Insets(8, 0, 5, 0);
		c.ipadx = width;
		c.gridx = 0;
		c.gridy = 4;
		JLabel lblFPSlave = new JLabel("Slave Directory Path");
		lblFPSlave.setForeground(Color.LIGHT_GRAY);
		lblFPSlave.setFont(new Font("Tahoma", Font.BOLD, 18));
		contentPane.add(lblFPSlave, c);
		// FilePath Slave
		c = new GridBagConstraints();
		c.fill = GridBagConstraints.HORIZONTAL;
		c.insets = new Insets(8, 0, 5, 0);
		c.ipadx = width;
		c.ipady = 8;
		c.gridx = 0;
		c.gridy = 5;
		tfFilePathSlave = new JTextField("M:\\iTunes");
		tfFilePathSlave.setForeground(new Color(0, 0, 0));
		tfFilePathSlave.setBackground(new Color(255, 255, 204));
		tfFilePathSlave.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if (e.getClickCount() == 2 && !e.isConsumed()) {
					e.consume();
					browseSlavePath();
				}
			}
		});
		contentPane.add(tfFilePathSlave, c);
		tfFilePathSlave.getDocument().addDocumentListener(new DocumentListener() {
			void changeSlaveDirectory() {
//				if ((new File(tfFilePathSlave.getText())).exists()) {
				currentPathSlave = tfFilePathSlave.getText().trim();
//				} else {
//					currentPathSlave = "";
//				}
//				System.out.println("currentPathSlave=" + currentPathSlave);
			}

			public void changedUpdate(DocumentEvent e) {
				changeSlaveDirectory();
			}

			public void removeUpdate(DocumentEvent e) {
				changeSlaveDirectory();
			}

			public void insertUpdate(DocumentEvent e) {
				changeSlaveDirectory();
			}
		});

		c = new GridBagConstraints();
		c.fill = GridBagConstraints.HORIZONTAL;
		c.insets = new Insets(8, 0, 5, 0);
		c.ipadx = width;
		c.ipady = 15;
		c.gridx = 0;
		c.gridy = 6;
		btnBrowseSlave = new JButton();
		btnBrowseSlave.setFont(new Font("Tahoma", Font.PLAIN, 14));
		btnBrowseSlave.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				browseSlavePath();
			}
		});

		btnBrowseSlave.setText("Browse for Slave Directory");
		contentPane.add(btnBrowseSlave, c);

		btnToggleSlave = new JButton("Toggle Recent");
		btnToggleSlave.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {

				if (recentDirectories.isEmpty())
					return;

				String directory = recentDirectories.get(recentDirectoryIteratorSlave);

//				if (new File(directory).exists()) {
				tfFilePathSlave.setText(directory);
				currentPathSlave = directory;
//				}

				if (recentDirectoryIteratorSlave >= recentDirectories.size() - 1) {
					recentDirectoryIteratorSlave = 0;
				} else {
					recentDirectoryIteratorSlave++;
				}
			}
		});
		btnToggleSlave.setHorizontalAlignment(SwingConstants.RIGHT);
		GridBagConstraints gbc_btnToggleSlave = new GridBagConstraints();
		gbc_btnToggleSlave.insets = new Insets(0, 0, 5, 0);
		gbc_btnToggleSlave.gridx = 0;
		gbc_btnToggleSlave.gridy = 7;
		contentPane.add(btnToggleSlave, gbc_btnToggleSlave);

		c = new GridBagConstraints();
		c.fill = GridBagConstraints.HORIZONTAL;
		c.insets = new Insets(24, 0, 5, 0);
		c.ipadx = width;
		c.gridx = 0;
		c.gridy = 8;
		JSeparator separator = new JSeparator();
		separator.setBackground(SystemColor.scrollbar);
		contentPane.add(separator, c);

		c = new GridBagConstraints();
		c.fill = GridBagConstraints.HORIZONTAL;
		c.insets = new Insets(24, 0, 5, 0);
		c.ipadx = width;
		c.ipady = 15;
		c.gridx = 0;
		c.gridy = 9;
		btnDeleteEmptyFolders = new JButton();
		btnDeleteEmptyFolders.setFont(new Font("Tahoma", Font.PLAIN, 14));
		btnDeleteEmptyFolders.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				if (!btnDeleteEmptyFolders.isEnabled()) {
					return;
				}
				if (DirectoryManager.getCurrent() != 0
						&& DirectoryManager.getCurrent() < directoryManager.getLengthOfTask()
						&& !DirectoryManager.isSynchronizeSwitchOn()
						&& btnDeleteEmptyFolders.getText().equals("Cancel")) {
					int choice = JOptionPane.showConfirmDialog(directoryManagerPresenter,
							"Are you sure you want to cancel now?");
					if (choice == JOptionPane.YES_OPTION) {
						mnSettings.setEnabled(true);
						btnToggleMaster.setEnabled(true);
						btnToggleSlave.setEnabled(true);
						DirectoryManager.setCanceledProgress(DirectoryManager.getCurrent());
						DirectoryManager.setCurrent(directoryManager.getLengthOfTask());
						btnDeleteEmptyFolders.setText("Delete Empty Folders in Master Directory");
						DirectoryManager.setUserCanceled(true);
					}
					return;
				}

				if (!new File(currentPathMaster).exists()
						|| (tfFilePathMaster.getText() == null || tfFilePathMaster.getText().trim().equals(""))) {
					JOptionPane.showMessageDialog(directoryManagerPresenter, "Please enter a valid Master Directory");
				}

				int choice = JOptionPane.showConfirmDialog(directoryManagerPresenter,
						"Are you sure? This will delete any empty folders within the directory '" + currentPathMaster
								+ "'!");
				if (choice == JOptionPane.YES_OPTION) {
					addNewRecentDirectoryIfNotExists(currentPathMaster);
					mnSettings.setEnabled(false);
					btnToggleMaster.setEnabled(false);
					btnToggleSlave.setEnabled(false);
					DirectoryManager.resetVariables();
					taskOutput.setText(" ");
					DirectoryManager.setDeleteFoldersWithOnlyHiddenImageFiles(
							cbItunesDeleteFoldersWithOnlyHiddenImageFiles.isSelected());
					DirectoryManager.setSynchronizeSwitchOn(false);
					DirectoryManager.setDeletedEmptyDirectoriesCounter(0);
					tfFilePathMaster.setEnabled(false);
					tfFilePathMaster.setEditable(false);
					btnBrowseMaster.setEnabled(false);
					tfFilePathSlave.setEnabled(false);
					tfFilePathSlave.setEditable(false);
					btnBrowseSlave.setEnabled(false);
					btnSynchronizeSlave.setEnabled(false);
					btnDeleteEmptyFolders.setText("Cancel");
					progressBar.setString("Calculating...");
					startTimeMillis = System.currentTimeMillis();
					Long totalFoldersCount = DirectoryManager.countTotalFoldersInDirectory(new File(currentPathMaster));
					DirectoryManager.setLengthOfTask(totalFoldersCount);
					DirectoryManager.setTotalFoldersCount(totalFoldersCount);
					progressBar.setMaximum(totalFoldersCount.intValue());
					jobFinishInitiated = false;
					directoryManager.go(new File(currentPathMaster), new File(currentPathSlave));
					timer.start();
					// DirectoryManager.searchAndDeleteEmptyFolders(new File(currentPathMaster));
					// JOptionPane.showMessageDialog(null,
					// DirectoryManager.getDeletedEmptyDirectoriesCounter() + " empty directories
					// deleted!");
				}
			}
		});

		btnDeleteEmptyFolders.setText("Delete Empty Folders in Master Directory");
		contentPane.add(btnDeleteEmptyFolders, c);

		c = new GridBagConstraints();
		c.fill = GridBagConstraints.HORIZONTAL;
		c.insets = new Insets(8, 0, 5, 0);
		c.ipadx = width;
		c.ipady = 10;
		c.gridx = 0;
		c.gridy = 10;
		btnSynchronizeSlave = new JButton();
		btnSynchronizeSlave
				.setIcon(new ImageIcon(DirectoryManagerPresenter.class.getResource("/pictures/icon_small.png")));
		btnSynchronizeSlave.setFont(new Font("Tahoma", Font.BOLD, 16));
		btnSynchronizeSlave.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				if (!btnSynchronizeSlave.isEnabled()) {
					return;
				}
				if ( // DirectoryManager.getCurrent() != 0 &&
				DirectoryManager.getCurrent() < directoryManager.getLengthOfTask()
						&& DirectoryManager.isSynchronizeSwitchOn() && btnSynchronizeSlave.getText().equals("Cancel")) {
					int choice = JOptionPane.showConfirmDialog(directoryManagerPresenter,
							"Are you sure you want to cancel now?");
					if (choice == JOptionPane.YES_OPTION) {
						mnSettings.setEnabled(true);
						btnToggleMaster.setEnabled(true);
						btnToggleSlave.setEnabled(true);
						DirectoryManager.setCanceledProgress(DirectoryManager.getCurrent());
						DirectoryManager.setCurrent(directoryManager.getLengthOfTask());
						btnSynchronizeSlave.setText("Synchronize Slave with Master");
						DirectoryManager.setUserCanceled(true);
					}
					return;
				}
				if (tfFilePathMaster.getText().equals(tfFilePathSlave.getText())) {
					JOptionPane.showMessageDialog(directoryManagerPresenter,
							"Slave Directory must not be the same as Master Directory!");
					return;
				}

				if (!new File(currentPathMaster).exists()
						|| (tfFilePathMaster.getText() == null || tfFilePathMaster.getText().trim().equals(""))) {
					JOptionPane.showMessageDialog(directoryManagerPresenter, "Please enter a valid Master Directory!");
					return;
				}

				if (!new File(currentPathSlave).exists()
						|| (tfFilePathSlave.getText() == null || tfFilePathSlave.getText().trim().equals(""))) {
					JOptionPane.showMessageDialog(directoryManagerPresenter, "Please enter a valid Slave Directory!");
					return;
				}

				synchronizationThread = new Thread() {
					public void run() {
						int choice = JOptionPane.showConfirmDialog(directoryManagerPresenter,
								"Are you sure?\nThis will synchronize all files and folders of the Slave Directory "
										+ "with the Master Directory \nand delete any files and folders that are currently "
										+ "on the Slave Directory \n(if they are not in the Master as well)\n\n"
										+ "Master Directory = '" + currentPathMaster + "'\n" + "Slave Directory = '"
										+ currentPathSlave + "'",
								"Synchronization", JOptionPane.YES_NO_CANCEL_OPTION);
						if (choice == JOptionPane.YES_OPTION) {

							addNewRecentDirectoryIfNotExists(currentPathSlave);
							addNewRecentDirectoryIfNotExists(currentPathMaster);

							btnSynchronizeSlave.setText("Cancel");
							progressBar.setString("Reading Master Directory...");
							// DirectoryManager.synchronizeMasterToSlave(new File(currentPathMaster), new
							// File(currentPathSlave));
							mnSettings.setEnabled(false);
							btnToggleMaster.setEnabled(false);
							btnToggleSlave.setEnabled(false);
							taskOutput.setText("");
							DirectoryManager.resetVariables();
							DirectoryManager.setSynchronizeSwitchOn(true);
							DirectoryManager.setUseTrashBin(cbMoveToTrash.isSelected());
							DirectoryManager.setSynchronizeNewestToMaster(cbAlsoSynchronizeBackToMaster.isSelected());
							taskOutput.setText("");
							btnDeleteEmptyFolders.setEnabled(false);
							tfFilePathMaster.setEnabled(false);
							tfFilePathMaster.setEditable(false);
							btnBrowseMaster.setEnabled(false);
							tfFilePathSlave.setEnabled(false);
							tfFilePathSlave.setEditable(false);
							btnBrowseSlave.setEnabled(false);
							startTimeMillis = System.currentTimeMillis();
							Long lengthOfTask = DirectoryManager.countFilesInDirectory(new File(currentPathMaster));
							progressBar.setMaximum(lengthOfTask.intValue());
							DirectoryManager.setLengthOfTask(lengthOfTask);
							jobFinishInitiated = false;

							directoryManager.go(new File(currentPathMaster), new File(currentPathSlave));
							timer.start();
						}

					}

				};
				synchronizationThread.start();
			}
		});

		btnSynchronizeSlave.setText("Synchronize Slave with Master");
		contentPane.add(btnSynchronizeSlave, c);

		c = new GridBagConstraints();
		c.fill = GridBagConstraints.HORIZONTAL;
		c.insets = new Insets(8, 0, 5, 0);
		c.ipadx = width;
		c.gridx = 0;
		c.ipady = 10;
		c.gridy = 11;
		progressBar = new JProgressBar(0, ((int) directoryManager.getLengthOfTask()));
		progressBar.setForeground(Color.GREEN);
		progressBar.setBackground(Color.DARK_GRAY);
		BasicProgressBarUI ui = new BasicProgressBarUI() {
			protected Color getSelectionBackground() {
				return Color.white; // string color over the background
			}

			protected Color getSelectionForeground() {
				return Color.black; // string color over the foreground
			}
		};
		progressBar.setUI(ui);
		progressBar.setStringPainted(true);
		progressBar.setString("");
		contentPane.add(progressBar, c);

		c = new GridBagConstraints();
		c.fill = GridBagConstraints.BOTH;
		c.weightx = 1.0;
		c.weighty = 1.0;
		c.ipadx = width;
		c.ipady = 120;
		c.gridx = 0;
		c.gridy = 12;
		c.gridheight = 2;
		c.insets = new Insets(paddingTop, 0, 0, 0);
		scrollPane = new JScrollPane();
		taskOutput = new JTextArea();
		taskOutput.setForeground(Color.WHITE);
		taskOutput.setBackground(Color.DARK_GRAY);
		taskOutput.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if (e.getClickCount() == 2 && !e.isConsumed()) {
					e.consume();
					logFrame.setLogText(taskOutput.getText() != null ? taskOutput.getText() : "");
					logFrame.setVisible(true);
				}
			}
		});
		taskOutput.setWrapStyleWord(true);

		scrollPane.setViewportView(taskOutput);
		taskOutput.setFont(new Font("Monospaced", Font.PLAIN, 12));
		taskOutput.setLineWrap(true);
		taskOutput.setEditable(false);
		taskOutput.setMargin(new Insets(5, 5, 5, 5));
		contentPane.add(scrollPane, c);

		setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
		setTitle("DirectorySynchronizR");
	}

	protected void browseMasterPath() {
		if (!btnBrowseMaster.isEnabled()) {
			return;
		}
		JFileChooser fileChooser = new JFileChooser();
		fileChooser.setCurrentDirectory(new File(currentPathMaster));
		fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		fileChooser.setVisible(true);
		JButton open = new JButton();
		while (fileChooser.isVisible()) {
			int option = fileChooser.showOpenDialog(open);
			if (option == JFileChooser.APPROVE_OPTION || option == JFileChooser.CANCEL_OPTION) {
				fileChooser.setVisible(false);
			}
		}
		if (fileChooser.getSelectedFile() != null) {
			tfFilePathMaster.setText(fileChooser.getSelectedFile().getAbsolutePath());
			currentPathMaster = fileChooser.getSelectedFile().getAbsolutePath();
		}
	}

	protected void browseSlavePath() {
		if (!btnBrowseSlave.isEnabled()) {
			return;
		}
		JFileChooser fileChooser = new JFileChooser();
		fileChooser.setCurrentDirectory(new File(currentPathSlave));
		fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		fileChooser.setVisible(true);
		JButton open = new JButton();
		while (fileChooser.isVisible()) {
			int option = fileChooser.showOpenDialog(open);
			if (option == JFileChooser.APPROVE_OPTION || option == JFileChooser.CANCEL_OPTION) {
				fileChooser.setVisible(false);
			}
		}
		if (fileChooser.getSelectedFile() != null) {
			tfFilePathSlave.setText(fileChooser.getSelectedFile().getAbsolutePath());
			currentPathSlave = fileChooser.getSelectedFile().getAbsolutePath();
		}

	}

	private static String getNewestVersionNumber() {
		StringBuffer htmlCode = new StringBuffer("");
		HttpURLConnection connection;
		try {
			connection = (HttpURLConnection) new URL(NEWEST_VERSION_URL).openConnection();
			connection.setRequestProperty("User-Agent",
					"Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.95 Safari/537.11");
			connection.setRequestProperty("Accept-Language", "de-DE,de;q=0.9,en-US;q=0.8,en;q=0.7");
			connection.setRequestProperty("Accept",
					"text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,image/jpg,image/jpeg,*/*;q=0.8");
			connection.connect();
			BufferedReader in = new BufferedReader(
					new InputStreamReader(connection.getInputStream(), Charset.forName("UTF-8")));

			String inputLine;
			htmlCode = new StringBuffer();
			while ((inputLine = in.readLine()) != null) {
				htmlCode.append(inputLine);
			}
			in.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

		String versionString = "";

		int pointer = htmlCode.indexOf("content=\"DirectorySynchronizR_");
		int firstPointer = pointer;
		String pointerValue = "";
		while (!pointerValue.toLowerCase().equals("v") && pointer < (firstPointer + 1000)) {
			pointer++;
			pointerValue = htmlCode.substring(pointer, pointer + 1);
		}
		int versionBeginIndex = pointer;
		while (!pointerValue.toLowerCase().equals(".jar") && pointer < (firstPointer + 1000)) {
			pointer++;
			pointerValue = htmlCode.substring(pointer, pointer + 4);
		}
		int versionEndIndex = pointer;
		versionString = htmlCode.substring(versionBeginIndex, versionEndIndex);
		return versionString;
	}

	public static long getStartTimeMillis() {
		return startTimeMillis;
	}

}
