package directorysynchronizr;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileFilter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.StringReader;
import java.nio.file.CopyOption;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import org.apache.commons.io.FileUtils;

public class DirectoryManager {
	private static final String FILE_SEPARATOR = System.getProperty("file.separator");
	private static int deletedEmptyDirectoriesCounter = 0;
	private static ArrayList<String> toDeletelist = new ArrayList<String>();
	private static Long totalFoldersCount = 0L;
	private static ArrayList<File> allMasterFiles;
	// private static Long allMasterFilesSize = 0L;
	// private static ArrayList<File> allSlaveFiles;
	private static long filesCopied = 0;
	private static long filesReplaced = 0;
	private static long filesReplacedInMaster = 0;
	private static long filesDeleted = 0;
	private static long canceledProgress = 0;
	private static String masterDirAbsolutePath;
	private static String slaveDirAbsolutePath;

	private static Long lengthOfTask = 1L;
	private static Long current = 0L;
	private static String statMessageBuffer = "";
	private static boolean useTrashBin = false;
	private static boolean synchronizeSwitchOn;
	private static boolean userCanceled;
	private static boolean synchronizeNewestToMaster = false;
	private static long copyTimeNeededInMillis;
	private static double copyAmountInKiloBytes;

	private static boolean emptyFolderDeleteJobDone;
	private static boolean deleteFoldersWithOnlyHiddenImageFiles = false;

	private static ArrayList<String> copiedMasterFiles = new ArrayList<String>();
	private static ArrayList<String> replacedSlaveFiles = new ArrayList<String>();
	private static ArrayList<String> deletedSlaveFiles = new ArrayList<String>();
	private static ArrayList<String> replacedMasterFiles = new ArrayList<String>();
	private static ArrayList<String> copyErrors = new ArrayList<String>();
	private static ArrayList<String> deletedDirectories = new ArrayList<String>();

	public static void startSearchAndDeleteEmptyFoldersJob(File directory) {
		while (searchAndDeleteEmptyFolders(directory)) {
			current -= countTotalFoldersInDirectory(directory);
		}
		emptyFolderDeleteJobDone = true;
	}

	private static boolean searchAndDeleteEmptyFolders(File directory) {
		if (userCanceled) {
			return false;
		}
		String[] fileList = directory.list();
		if (current % 100 == 0) {
			System.out.print(current + " of " + totalFoldersCount + "; ");
			if (current % 500 == 0) {
				System.out.println();
			}
		}
		boolean onlyHiddenFiles = false;
		if (deleteFoldersWithOnlyHiddenImageFiles) {
			onlyHiddenFiles = true;
			for (String currFilePath : fileList) {
				File currFile = new File(directory.getAbsolutePath() + FILE_SEPARATOR + currFilePath);
				if (currFile.isDirectory() || !currFile.isHidden() || !currFilePath.endsWith(".jpg")) {
					onlyHiddenFiles = false;
				}
			}
		}
		if ((fileList.length == 0 || (onlyHiddenFiles && deleteFoldersWithOnlyHiddenImageFiles))
				&& !toDeletelist.contains(directory.getAbsolutePath())) {
			System.out.println();
			deleteEmptyDirectory(directory);
			deletedDirectories.add(directory.getAbsolutePath());
			if (current != 0) {
//				appendToMessageBuffer(current + ". of " + lengthOfTask + " directories. Deleted. ('"
//						+ directory.getAbsolutePath() + "')");
			}
//			try {
//				Thread.sleep(2);
//			} catch (InterruptedException e) {
//				e.printStackTrace();
//			}
			return true;
		} else {
			if (current != 0) {
//				appendToMessageBuffer(current + ". of " + lengthOfTask + " directories. Current: '"
//						+ directory.getAbsolutePath() + "'");
			}
		}
		current++;
		boolean foldersDeletedTemp = true;
		boolean foldersDeleted = false;
		while (foldersDeletedTemp) {
			if (userCanceled) {
				return false;
			}
			foldersDeletedTemp = false;
			for (int i = 0; i < fileList.length; i++) {
				File currentFile = new File(directory.getPath().trim() + FILE_SEPARATOR + fileList[i]);
				if (currentFile.isDirectory()) {
					if (searchAndDeleteEmptyFolders(currentFile)) {
						foldersDeletedTemp = true;
						foldersDeleted = true;
					}
				}
			}
			if (foldersDeletedTemp) {
				current -= countTotalFoldersInDirectory(directory);
			}
		}
		if (foldersDeleted) {
			return true;
		}
		return false;
	}

	private static void deleteEmptyDirectory(File emptyDirectory) {
		String[] fileList = emptyDirectory.list();
		if (!deleteFoldersWithOnlyHiddenImageFiles && fileList.length != 0) {
			return;
		}

		boolean onlyHiddenFiles = false;
		if (deleteFoldersWithOnlyHiddenImageFiles) {
			onlyHiddenFiles = true;
			for (String currFilePath : fileList) {
				File currFile = new File(emptyDirectory.getAbsolutePath() + FILE_SEPARATOR + currFilePath);
				if (currFile.isDirectory() || !currFile.isHidden() || !currFilePath.endsWith(".jpg")) {
					onlyHiddenFiles = false;
				}
			}
			if (!onlyHiddenFiles) {
				return;
			}
		}

		if (emptyDirectory.exists() && emptyDirectory.isDirectory()) {
			if (!toDeletelist.contains(emptyDirectory.getAbsolutePath())) {
				toDeletelist.add(emptyDirectory.getAbsolutePath());
				System.out.println("Deleting '" + emptyDirectory.getAbsolutePath() + "'");
				try {
					FileUtils.deleteDirectory(emptyDirectory);
					deletedEmptyDirectoriesCounter++;
				} catch (IOException e) {
					e.printStackTrace();
				}
				// emptyDirectory.delete();

			}
		}
	}

	public static int getDeletedEmptyDirectoriesCounter() {
		return deletedEmptyDirectoriesCounter;
	}

	public static void setDeletedEmptyDirectoriesCounter(int deletedEmptyDirectoriesCounter) {
		DirectoryManager.deletedEmptyDirectoriesCounter = deletedEmptyDirectoriesCounter;
	}

	public static void synchronizeMasterToSlave(File masterDir, File slaveDir) {
		resetVariables();
		masterDirAbsolutePath = masterDir.getAbsolutePath();
		slaveDirAbsolutePath = slaveDir.getAbsolutePath();
		allMasterFiles = getFilesArrayListFromDirectoryFile(masterDir);
		// allSlaveFiles = getFilesArrayListFromDirectoryFile(slaveDir);
		copyAllMasterFilesCascadingAndCleanSlaveDir(new ArrayList<File>(allMasterFiles), masterDir);
	}

	public static void resetVariables() {
		statMessageBuffer = "";
		current = 0L;
		filesCopied = 0;
		filesReplaced = 0;
		filesReplacedInMaster = 0;
		filesDeleted = 0;
		toDeletelist.clear();
		userCanceled = false;
		canceledProgress = 0;
		emptyFolderDeleteJobDone = false;
		copyTimeNeededInMillis = 0;
		copyAmountInKiloBytes = 0;

		copiedMasterFiles.clear();
		replacedSlaveFiles.clear();
		deletedSlaveFiles.clear();
		replacedMasterFiles.clear();
		deletedDirectories.clear();
		copyErrors.clear();

	}

	private static ArrayList<File> getFilesArrayListFromDirectoryFile(File file) {

		String[] currentPathFileNames = file.list();
		ArrayList<File> resultList = new ArrayList<File>();
		if (!file.isDirectory() || currentPathFileNames == null) {
			return null;
		}
		for (String currentFilePath : currentPathFileNames) {
			resultList.add(new File(file.getAbsolutePath() + FILE_SEPARATOR + currentFilePath));
		}
		return resultList;
	}

	private static ArrayList<String> copyAllMasterFilesCascadingAndCleanSlaveDir(
			ArrayList<File> allMasterFilesCurrentLevel, File currentLevelMasterDir) {
		if (userCanceled) {
			return null;
		}
		for (File currentFile : allMasterFilesCurrentLevel) {
			if (userCanceled) {
				return null;
			}
			if (currentFile.isDirectory()) {
//				appendToMessageBuffer((current + 1) + ". of " + lengthOfTask + " paths. Synchronizing directory '"
//						+ currentFile.getAbsolutePath() + "'");

				if (!allMasterFiles.contains(currentFile)) {
					allMasterFiles.add(currentFile);
				}
				File currentSlaveFile = new File(slaveDirAbsolutePath
						+ currentFile.getAbsolutePath().split(masterDirAbsolutePath.replace("\\", "\\\\"))[1]);
				if (!currentSlaveFile.exists()) {
					currentSlaveFile.mkdirs();
				}
				ArrayList<File> filesArrayListFromDirectoryFile = getFilesArrayListFromDirectoryFile(currentFile);
				if (filesArrayListFromDirectoryFile != null) {
					copyAllMasterFilesCascadingAndCleanSlaveDir(filesArrayListFromDirectoryFile, currentFile);
				}
			} else {
				allMasterFiles.add(currentFile);

				String slaveFilePath = slaveDirAbsolutePath
						+ currentFile.getAbsolutePath().split(masterDirAbsolutePath.replace("\\", "\\\\"))[1];
				File slaveFile = new File(slaveFilePath);
				boolean masterFileExistsInSlaveDirAndIsEqual = masterFileExistsInSlaveDirAndIsEqual(currentFile,
						slaveFile);
				boolean slaveFileIsNewerThanMasterFile = slaveFileIsNewerThanMasterFile(currentFile, slaveFile);
				if (!masterFileExistsInSlaveDirAndIsEqual
						&& (!(slaveFileIsNewerThanMasterFile && synchronizeNewestToMaster))) {
					boolean isReplaced = false;
					if (slaveFile.exists()) {
						if (useTrashBin) {
							moveToTrash(slaveFile);
						}
						isReplaced = true;
					}
					try {
						long copyStartTime = System.currentTimeMillis();
						copyAmountInKiloBytes += (((double) currentFile.length()) / 1000.0);
						copyOrReplaceFile(currentFile, slaveFile);
						copyTimeNeededInMillis += System.currentTimeMillis() - copyStartTime;

						if (isReplaced) {
							filesReplaced++;
							replacedSlaveFiles.add(slaveFile.getAbsolutePath());
						} else {
							filesCopied++;
							copiedMasterFiles.add(currentFile.getAbsolutePath());
						}
					} catch (IOException e) {
						e.printStackTrace();
						copyErrors.add(slaveFile.getAbsolutePath());
					}
//					appendToMessageBuffer((current + 1) + ". of " + lengthOfTask
//							+ (isReplaced ? " paths. Replaced in Slave. '" : " paths. Copied. '")
//							+ currentFile.getAbsolutePath() + "'");
//					try {
//						Thread.sleep(2);
//					} catch (InterruptedException e) {
//						e.printStackTrace();
//					}
				} else if (slaveFileIsNewerThanMasterFile && synchronizeNewestToMaster) {
					if (useTrashBin) {
						moveToTrash(currentFile);
					}
					try {
						long copyStartTime = System.currentTimeMillis();
						copyAmountInKiloBytes += (((double) currentFile.length()) / 1000.0);
						copyOrReplaceFile(slaveFile, currentFile);
						copyTimeNeededInMillis += System.currentTimeMillis() - copyStartTime;
						filesReplacedInMaster++;
						replacedMasterFiles.add(currentFile.getAbsolutePath());
					} catch (IOException e) {
						e.printStackTrace();
						copyErrors.add(currentFile.getAbsolutePath());
					}
//					appendToMessageBuffer((current + 1) + ". of " + lengthOfTask + " paths. Replaced Master File '"
//							+ currentFile.getAbsolutePath() + "'");

//					try {
//						Thread.sleep(2);
//					} catch (InterruptedException e) {
//						e.printStackTrace();
//					}
				} else {
//					appendToMessageBuffer((current + 1) + ". of " + lengthOfTask + " paths. In sync. '"
//							+ currentFile.getAbsolutePath() + "'");
				}
				current++;
			}

		}
		File slaveDirCurrentLevel = null;
		if (!currentLevelMasterDir.getAbsolutePath().equals(masterDirAbsolutePath)) {
			slaveDirCurrentLevel = new File(slaveDirAbsolutePath
					+ currentLevelMasterDir.getAbsolutePath().split(masterDirAbsolutePath.replace("\\", "\\\\"))[1]);

		} else {
			slaveDirCurrentLevel = new File(slaveDirAbsolutePath);
		}
		ArrayList<File> allSlaveFilesCurrentLevel = getFilesArrayListFromDirectoryFile(slaveDirCurrentLevel);

		if (allSlaveFilesCurrentLevel != null) {
			for (File currSlaveFile : allSlaveFilesCurrentLevel) {
				File notExistingMasterFileLikeSlaveFile = new File(masterDirAbsolutePath
						+ currSlaveFile.getAbsolutePath().split(slaveDirAbsolutePath.replace("\\", "\\\\"))[1]);
				if (!notExistingMasterFileLikeSlaveFile.exists()) {
					if (currSlaveFile.isDirectory()) {
						try {
							FileUtils.deleteDirectory(currSlaveFile);
						} catch (IOException e) {
							e.printStackTrace();
						}
					} else {
						if (useTrashBin) {
							moveToTrash(currSlaveFile);
						} else {
							currSlaveFile.delete();
						}
					}
//					appendToMessageBuffer("Deleted Slave File '" + currSlaveFile.getAbsolutePath() + "'");
					filesDeleted++;
					deletedSlaveFiles.add(currSlaveFile.getAbsolutePath());
				}

			}
		}

		return null;
	}

	private static boolean slaveFileIsNewerThanMasterFile(File masterFile, File slaveFile) {
		if (slaveFile.exists()) {
			if (slaveFile.lastModified() > masterFile.lastModified()) {
				return true;
			}
		}
		return false;
	}

	private static void copyOrReplaceFile(File source, File dest) throws IOException {
		CopyOption[] options = new CopyOption[] { StandardCopyOption.REPLACE_EXISTING,
				StandardCopyOption.COPY_ATTRIBUTES };
		Files.copy(Paths.get(source.getAbsolutePath()), Paths.get(dest.getAbsolutePath()), options);
	}

	private static boolean masterFileExistsInSlaveDirAndIsEqual(File masterFile, File slaveFile) {
		if (slaveFile.exists()) {
			if (slaveFile.length() == masterFile.length() && slaveFile.lastModified() == masterFile.lastModified()) {
				return true;
			}
		}
		return false;
	}

	public static void printAllMasterFiles() {
		for (File currFile : allMasterFiles) {
			System.out.println(currFile.getAbsolutePath());
		}
	}

	// Progressbar-Code

	/**
	 * Called from Frontend to start the task.
	 */
	void go(File masterDir, File slaveDir) {
		current = 0L;
		final SwingWorker worker = new SwingWorker() {
			public Object construct() {
				return new ActualTask(masterDir, slaveDir);
			}
		};
		worker.start();
	}

	/**
	 * Called from Frontend to find out how much work needs to be done.
	 */
	long getLengthOfTask() {
		return lengthOfTask;
	}

	/**
	 * Called from Frontend to find out how much has been done.
	 */
	public static long getCurrent() {
		return current;
	}

	static void setCurrent(long newCurrent) {
		current = newCurrent;
	}

	void stop() {
		appendToMessageBuffer("Canceled by user.");
		current = lengthOfTask;
	}

	/**
	 * Called from Frontend to find out if the task has completed.
	 */
	static boolean done() {
		if (current >= lengthOfTask) {
			if (!synchronizeSwitchOn) {
				return emptyFolderDeleteJobDone;
			}
			return true;
		} else {
			return false;
		}
	}

	static void appendToMessageBuffer(String message) {
//		if (message.contains("paths."))
//			return;

		if (statMessageBuffer.isEmpty()) {
			statMessageBuffer += message;
		} else {
			statMessageBuffer += "\n" + message;
		}
	}

	static String getMessageBuffer() {
		return statMessageBuffer;
	}

	static void clearMessageBuffer() {
		statMessageBuffer = "";
	}

	public static long getFilesCopied() {
		return filesCopied;
	}

	public static long getFilesReplaced() {
		return filesReplaced;
	}

	public static long getFilesReplacedInMaster() {
		return filesReplacedInMaster;
	}

	public static long getFilesDeleted() {
		return filesDeleted;
	}

	/**
	 * The actual long running task. This runs in a SwingWorker thread.
	 */
	class ActualTask {
		ActualTask(File masterDir, File slaveDir) {
			if (synchronizeSwitchOn) {
				// lengthOfTask = countFilesInDirectory(masterDir);
				synchronizeMasterToSlave(masterDir, slaveDir);
				if (current > lengthOfTask) {
					current = lengthOfTask;
				}
			} else {
				// lengthOfTask = countTotalFoldersInDirectory(masterDir);
				System.out.println("lot: " + lengthOfTask);
				// searchAndDeleteEmptyFolders(masterDir);
				startSearchAndDeleteEmptyFoldersJob(masterDir);
				if (current > lengthOfTask) {
					current = lengthOfTask;
				}
			}
			// statMessage = "Completed. " + current + " out of " + lengthOfTask + " files
			// processed.";
			try {
//				Thread.sleep(5);
				Thread.sleep(500);
			} catch (InterruptedException e) {
			}
		}
	}

	// Progressbar-Code End

	/**
	 * Count files in a directory (including files in all subdirectories)
	 * 
	 * @param directory the directory to start in
	 * @return the total number of files
	 */
	public static Long countFilesInDirectory(File directory) {
		Long count = 0L;
		for (File file : directory.listFiles()) {
			if (file.isFile()) {
				count++;
			}
			if (file.isDirectory()) {
				count += countFilesInDirectory(file);
			}
		}
		return count;
	}

	public static Long countTotalFoldersInDirectory(File directory) {
		Long count = 0L;
		File[] folderList = directory.listFiles(new FileFilter() {
			@Override
			public boolean accept(File f) {
				return f.isDirectory();
			}
		});

		if (folderList == null || folderList.length < 1) {
			return 0L;
		}
		for (File file : folderList) {
			// System.out.println(file.getAbsolutePath());
			count++;
			count += countTotalFoldersInDirectory(file);
		}
		return count;

	}

	private static void moveToTrash(File file) {
		com.sun.jna.platform.FileUtils fileUtils = com.sun.jna.platform.FileUtils.getInstance();
		if (fileUtils.hasTrash()) {
			try {
				System.out.println("Moving to trash.. " + file.getAbsolutePath());
				fileUtils.moveToTrash(new File[] { file });
			} catch (IOException ioe) {
				ioe.printStackTrace();
			}
		} else {
			System.out.println("No Trash available");
		}
	}

	public static void setTotalFoldersCount(Long totalFoldersCount) {
		DirectoryManager.totalFoldersCount = totalFoldersCount;
	}

	public static void setUseTrashBin(boolean useTrashBin) {
		DirectoryManager.useTrashBin = useTrashBin;
	}

	public static void setSynchronizeSwitchOn(boolean synchronizeSwitchOn) {
		DirectoryManager.synchronizeSwitchOn = synchronizeSwitchOn;
	}

	public static boolean isSynchronizeSwitchOn() {
		return synchronizeSwitchOn;
	}

	public static void setLengthOfTask(Long lengthOfTask) {
		DirectoryManager.lengthOfTask = lengthOfTask;
	}

	public static void setUserCanceled(boolean userCanceled) {
		DirectoryManager.userCanceled = userCanceled;
	}

	public static boolean isUserCanceled() {
		return userCanceled;
	}

	public static long getCanceledProgress() {
		return canceledProgress;
	}

	public static void setCanceledProgress(long canceledProgress) {
		DirectoryManager.canceledProgress = canceledProgress;
	}

	public static void setDeleteFoldersWithOnlyHiddenImageFiles(boolean deleteFoldersWithOnlyHiddenImageFiles) {
		DirectoryManager.deleteFoldersWithOnlyHiddenImageFiles = deleteFoldersWithOnlyHiddenImageFiles;
	}

	public static boolean isEmptyFolderDeleteJobDone() {
		return emptyFolderDeleteJobDone;
	}

	public static double getCurrentAverageCopySpeedInKiloBytesPerSecond() {
		return (double) (copyAmountInKiloBytes / (((double) copyTimeNeededInMillis) / 1000.0));
	}

	public static void setSynchronizeNewestToMaster(boolean synchronizeNewestToMaster) {
		DirectoryManager.synchronizeNewestToMaster = synchronizeNewestToMaster;
	}

	public static String getTimeLeftString() {
		if (current == 0) {
			return "";
		}
		Long currentTimeNeededMillis = System.currentTimeMillis() - DirectoryManagerPresenter.getStartTimeMillis();

		Long timeNeededForAllMillis = (long) ((currentTimeNeededMillis.doubleValue() / current.doubleValue())
				* lengthOfTask.doubleValue());
		Long timeLeftSeconds = (timeNeededForAllMillis - currentTimeNeededMillis) / 1000;
		Long timeLeftOnlySeconds = timeLeftSeconds % 60;

		// return "(time left: " + timeLeftSeconds + " secs)";
		Long timeLeftOnlyMinutes = ((timeLeftSeconds - (timeLeftSeconds % 60)) / 60) % 60;
		Long timeLeftOnlyHours = 0L;
		if (((timeLeftSeconds - (timeLeftSeconds % 60)) / 60) > 60) {
			timeLeftOnlyHours = (((timeLeftSeconds - (timeLeftSeconds % 60)) / 60) - timeLeftOnlyMinutes) / 60;
		}
		String timeLeftOnlyHoursString = timeLeftOnlyHours > 9 ? timeLeftOnlyHours.toString() : "0" + timeLeftOnlyHours;
		String timeLeftOnlyMinutesString = timeLeftOnlyMinutes > 9 ? timeLeftOnlyMinutes.toString()
				: "0" + timeLeftOnlyMinutes;
		String timeLeftOnlySecondsString = timeLeftOnlySeconds > 9 ? timeLeftOnlySeconds.toString()
				: "0" + timeLeftOnlySeconds;

		return "(time left: " + timeLeftOnlyHoursString + ":" + timeLeftOnlyMinutesString + ":"
				+ timeLeftOnlySecondsString + ")";
	}

	public static String getCompletionSummaryAndWriteLogFile() {

		String logsFolderPath = DirectoryManagerPresenter.appFolderPath + FILE_SEPARATOR
				+ DirectoryManagerPresenter.LOGS_FOLDER_NAME;

		if (!new File(logsFolderPath).exists()) {
			try {
				Files.createDirectories(new File(logsFolderPath).toPath());
			} catch (IOException e) {
				e.printStackTrace();
			}
			System.out.println(logsFolderPath);
		}

		File logFile = new File(logsFolderPath + FILE_SEPARATOR
				+ (isSynchronizeSwitchOn() ? "Synchronize-Log_" : "Directory-Cleanup-Log_") + getTimeStampString()
				+ ".txt");
		String completionSummary = getCompletionSummary();

		FileOutputStream fos = null;
		BufferedReader br = null;
		try {
			fos = new FileOutputStream(logFile, true);
			br = new BufferedReader(new StringReader(completionSummary));

			String line = null;
			while ((line = br.readLine()) != null) {
				fos.write((line + "\n").getBytes());
			}

			br.close();
			fos.close();
		} catch (Exception e) {
			e.printStackTrace();
			try {
				if (fos != null)
					fos.close();

				if (br != null)
					br.close();

			} catch (Exception e1) {
			}
		}
		return completionSummary;
	}

	public static String getCompletionSummary() {
		String summary = "";

		if (synchronizeSwitchOn) {
			summary = "\n\n__________________________________\n\nSynchronization Summary\n";
		} else {
			summary = "\n\n__________________________________\n\nEmpty Folders Cleaning Summary\n";
		}
		summary += "__________________________________\n\n";

		summary += "Master Folder Path=" + masterDirAbsolutePath + "\n";
		summary += " Slave Folder Path=" + slaveDirAbsolutePath + "\n";
		summary += "__________________________________\n\n\n";

		int lengthAtStart = summary.length();

		if (copiedMasterFiles.size() > 0) {
			summary += "\n" + copiedMasterFiles.size() + " Copied Master Files:\n";
			for (String filePath : copiedMasterFiles) {
				summary += filePath + "\n";
			}
		}

		if (replacedSlaveFiles.size() > 0) {
			summary += "\n" + replacedSlaveFiles.size() + " Updated Slave Files:\n";
			for (String filePath : replacedSlaveFiles) {
				summary += filePath + "\n";
			}
		}

		if (replacedMasterFiles.size() > 0) {
			summary += "\n" + replacedMasterFiles.size() + " Updated Master Files:\n";
			for (String filePath : replacedMasterFiles) {
				summary += filePath + "\n";
			}
		}

		if (deletedSlaveFiles.size() > 0) {
			summary += "\n" + deletedSlaveFiles.size() + " Deleted Slave Files:\n";
			for (String filePath : deletedSlaveFiles) {
				summary += filePath + "\n";
			}
		}

		if (deletedDirectories.size() > 0) {
			summary += "\n" + deletedDirectories.size() + " Deleted Master Directories:\n";
			for (String filePath : deletedDirectories) {
				summary += filePath + "\n";
			}
		}

		if (copyErrors.size() > 0) {
			summary += "\n" + copyErrors.size() + " Copy Errors:\n";
			for (String filePath : copyErrors) {
				summary += filePath + "\n";
			}
		}

		if (lengthAtStart == summary.length()) {
			summary += synchronizeSwitchOn ? "Master and Slave were in sync. Nothing has been synchronized.\n"
					: "Master seemed clean of empty folders. No folders were deleted.\n";
		}

		summary += "__________________________________";
		return summary;
	}

	public static String getTimeStampString() {
		return new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss").format(new Date());
	}
}
